
import urllib2
import re
from htmldom.htmldom import HtmlDom
from dal import DAL,Field
from datetime import datetime
from dateutil import parser
from hashlib import md5
import subprocess
import argparse
import platform
import os
import wcp
import sys

class WCPDB( object ):
   def __init__(self):
        self.connection = DAL("sqlite://storage.db")
        self.connection.define_table("wcp_crawls", 
            Field("wcp_page_uri"),
	    Field("wcp_page_modified"))
            
   def save(self,*args,**kwargs):
        return self.connection.wcp_crawls.insert( **kwargs )
class WCPLog( object ):
   def __init__(self):
       pass
   def log(self,message):
       logPath = WCPUtility.getLogPath()
       logFile = open( logPath, "a" )
       now = datetime.now()
       msg = "WCP LOG: {} - {}".format( message, now.strftime("%Y-%m-%d" ))
       logFile.write(msg)
       logFile.close()
       ## also print
       print msg
 
 
class WCPUtility( object ):
   @staticmethod
   def forwardIterator(string,delimeter,pos=0,_continue=False):
      stringIterator = 0
      finds = 0
      chunksz=len(delimeter)
      if not _continue:
	      while stringIterator != len(string):
		  chunk= string[stringIterator:stringIterator+chunksz]
		  if  chunk==delimeter and (pos==finds):
		    return string[0:stringIterator]
		  elif chunk==delimeter:
		    finds+=1
		  stringIterator+=1
      else:
	       foundIterator=False
	       while stringIterator <= len(string):
		   chunk = string[stringIterator:stringIterator+chunksz]
		   if  chunk == delimeter:
		       if pos == finds:	
	 	          foundIterator=True
		       else:
			   finds += 1
		   else:
		        if foundIterator:
		 	    return string[stringIterator:len(string)]
	 	   stringIterator +=1

      return string

   @staticmethod
   def continueIterator(string,delimeter,pos=0):

	 return WCPUtility.forwardIterator(string,delimeter,pos=pos,_continue=True)
      
   @staticmethod
   def backwardsIterator(string,delimeter,_continue=False):
      stringIterator = len(string)-1
      chunksz =  len(delimeter)
  
      while stringIterator > 0:
	 chunk =  string[(stringIterator-chunksz):stringIterator]
	 if chunk==delimeter:
	     return  string[0:stringIterator]
	 stringIterator-=1
      return ""

   @staticmethod
   def countIterator(string,delimeter):
      stringIterator = 0
      chunksz =  len( delimiter )
      count = 0
      return
   @staticmethod
   def backwardsIteratorContents(string,delimeter):
      stringIterator = len(string)-1
      contents = ""
      while stringIterator > 0 :
         if  string[stringIterator]==delimeter:
            return contents[::-1]
         else:
            contents += string[stringIterator]
         stringIterator-=1
      return contents[::-1]
   
     

   @staticmethod
   def stripURL(url):
       return WCPUtility.backwardsIterator(url,"/")
   @staticmethod
   def stripFilename(url):
       return WCPUtility.backwardsIteratorContents(url, "/")

   ##both should be loaded
   @staticmethod
   def cmpDate( date1, date2 ):
       timestamp1 = time.mktime( date1.timetuple() )
       timestamp2 = time.mktime( date2.timetuple() )
       if timestamp1 != timestamp2:
	   return 1
       return 0
       
   @staticmethod
   def getDate( dateString ):
       return parser.parse(dateString)
       

   @staticmethod
   def getDownloadPath():
      return os.getcwd()+'/downloads/'
   @staticmethod
   def getLogPath():
      return os.getcwd()+'/logs/log.txt'
   @staticmethod
   def getLogger():
      return wcp.logger
   @staticmethod
   def getBasePath(entrypoint):
      return WCPUtility.continueIterator(WCPUtility.forwardIterator(entrypoint, "/", pos=3), ".", 0)
   @staticmethod
   def getProtocol(entrypoint):
      return WCPUtility.forwardIterator(entrypoint, "/", pos=0)+"//"
   @staticmethod
   def getDomainPath(entrypoint): 
      return WCPUtility.forwardIterator(WCPUtility.continueIterator(entrypoint, "/", pos=1), "/")

   ## only support www, TODO: changeme 
   @staticmethod
   def getDomainPathAbs(entrypoint):
      domain = WCPUtility.getDomainPath(entrypoint)
      prefix =  WCPUtility.forwardIterator(domain, ".", pos=1)
      if domain.startswith("www"):
          return WCPUtility.continueIterator(domain, ".", pos=0)
      return domain
 
   @staticmethod 
   def getRelPath(entrypoint):
      return WCPUtility.backwardsIterator(WCPUtility.continueIterator(entrypoint, "/", pos=1), "/")
   @staticmethod
   def getQualifiedPathV1( path ):
		  beforeDelimiter =  WCPUtility.forwardIterator(path, "/../")
		  afterDelimiter = WCPUtility.backwardIterator(path, "/../")
		  countDelimiter = WCPUtility.countIterator(path, "/../") 
		  absDirectory =  WCPUtility.backwardIterator(beforeDelimiter,countDelimiter)
		  fullDirectory = absDirectory+afterDelimter
		  return protocol+basePath+fullDirectory 
   @staticmethod
   def getQualifiedPathV2(path):
       pathString = "/../"
       while pathString in path:
	    stringIterator =0
	    chunk = ""
	    len_of_string =len(path)
	    beforeString = ""
   	    afterString = ""
	    countOfSlashes = 0
	    while stringIterator != len_of_string:
		chunk += path[stringIterator]
		clause1 = chunk == pathString
		clause2 = path[stringIterator] == "/"
		if clause1:
			pathNeeded =  WCPUtility.forwardIterator(path, "/", pos=countOfSlashes-2)+"/"
			path = pathNeeded+path[stringIterator+1:len_of_string]
			before_len = len_of_string
			len_of_string =  len( path )
			stringIterator = stringIterator-(before_len-len_of_string)
			
		elif clause2:
			countOfSlashes += 1	
		if len( chunk ) > len( pathString ):
			chunk = ""
	    	stringIterator +=1		
       return path
	    
		
			
		 
				

			
			
				
	 

   @staticmethod
   def getQualifiedPath( path ):
	 return WCPUtility.getQualifiedPathV2(path)


   @staticmethod
   def getFullPath(entrypoint, path):
      protocol = WCPUtility.getProtocol(entrypoint)
      pathProtocol = WCPUtility.getProtocol(path)
      domainPath = WCPUtility.getDomainPath(entrypoint)
      relPath = WCPUtility.getRelPath(entrypoint)

      forwardPath = WCPUtility.forwardIterator(path, "/")

      if  not path[:1]=="#":
         path= WCPUtility.getQualifiedPath( path )

         if path[:1]=="/":
            return [True, protocol+domainPath+path]
         elif path[:2]=="./":
            return [True,protocol+relPath+path]
         else: 
	     if not pathProtocol=="http://"and not pathProtocol=="https://":
		return [True,protocol+domainPath+"/"+path]
         return [True, path]
      return [False, path]
       
       

   @staticmethod
   def getFilenameAndExtension(url):
      filename = WCPUtility.backwardsIteratorContents(url, "/")
      split =  filename.split(".")
      if  len(split)>1:
         return split
      else:
	 if filename == "": ## avoid empties, default to index
	    filename = "index"
         return [filename, "noextension"]
  
   @staticmethod
   def getFileName(url):
      pass

   @staticmethod
   def getFileExtension(url):
      pass



   @staticmethod
   def getFullDownloadName(url):
       now = datetime.now()
       dateString = now.strftime("%Y-%m-%d")
       base = WCPUtility.getDomainPathAbs(url)
       directoryPath = WCPUtility.backwardsIterator(WCPUtility.continueIterator(url, "/", pos=2), "/")
       fileNameAndExtension = WCPUtility.getFilenameAndExtension(url)
       fileName = fileNameAndExtension[0]
       extension =  fileNameAndExtension[1]
       logger = WCPUtility.getLogger()

       logger.log("Trying to create base: {0}, path: {1}, file: {2}".format(base, directoryPath, fileName))
       WCPUtility.createDirectoriesAsNeededAndReturn(
		base=base,
		directoryPath=directoryPath)
       fullFileName =  "{}/{}{}-{}.{}".format(base,directoryPath,fileName, dateString,extension)
       logger.log("Determined file path: {0}".format(fullFileName))
       return fullFileName
   @staticmethod
   def createDirectoriesAsNeededAndReturn(base="", directoryPath=""):
	 ## make base directory if needed
	 ##  
	 fullBasePath = WCPUtility.getDownloadPath()+"/"+base
	 logger = WCPUtility.getLogger()

	 if not os.path.isdir(fullBasePath):
	     logger.log("Creating directory as it does not exist {0}".format(fullBasePath))
	     os.makedirs(fullBasePath)

	 splittedDirectories = directoryPath.split("/")
 	 directoryString = ""
	 for i in  splittedDirectories:
	     thisDirectory = fullBasePath+"/"+directoryString+"/"+i
	     if not os.path.isdir( thisDirectory ):
		  logger.log("Creating directory as it does not exist {0}".format(thisDirectory))
	 	  os.makedirs( thisDirectory )
	     directoryString += i
 
	     

  
		

       
## get all data from provided urls
##
class WCPCrawler( object ):
   def __init__(self,  entrypoint='', lastmodified=False):
       (status, result) =  WCPUtility.getFullPath(entrypoint, "")
       
       self.base = result
       self.lastmodified=lastmodified
       self.db = WCPDB()
       self.downloader = WCPDownloader( self.db )
       self.log = WCPUtility.getLogger()
       self.crawled = []
   def crawl_all_links(self):
       response = WCPRequest.request( self.base )
       document = WCPDocument( response)
       self._crawl_all_links( document )
  
   def _add_to_crawled(self, link): 
       self.crawled.append( link.response.uri )
   def _cmp_base_and_anchor(self, predicate): 
       strip1 = WCPUtility.getDomainPathAbs( self.base )
       strip2 = WCPUtility.getDomainPathAbs( predicate )
       return True if strip1==strip2 else False

    
   def _crawl_all_links(self, document):
       if  document.valid:
           self._each_result( document )
           self._add_to_crawled( document )

	   if document.dom:
		   anchors = document.dom.find("a")
		   len =anchors.length() 
		   for i in range(0,len-1):
		       anchor = anchors[i]
		       pathBefore = anchor.attr("href")
		       (status, href) = WCPUtility.getFullPath(document.response.uri, anchor.attr("href"))
		       if status:
			       if not href in self.crawled and href:
				    
				   self.log.log("Scanning Before HREF transformation {}, after transformation: {}".format(pathBefore, href))
				   if self._cmp_base_and_anchor( href ):
				      self.log.log("Base URL was met, checking document for updates {}".format(href)) 
				      response = WCPRequest.request(href)
				      childDocument = WCPDocument( response)
				      self._crawl_all_links( childDocument )
				   else:
				      self.log.log("Base URL was not met {}".format(href))
			       else:
				   self.log.log("URL {} was already crawled".format(href))
		       else:
				self.log.log("URL could not be parsed. Error in".format(href))
       else:
           self.log.log("Unable to determine DOM of {}".format(document.response.uri))
            
   
   def _request(self, uri):
       self.log.log("Requesting {}".format(uri))
       req = urllib2.Request(uri)
       res = urllib2.urlopen(req)
       status = res.getcode()
       data = res.read()## needs root element
       response = WCPResponse(status=status,data=data,headers=res.getheaders())  
       self.log.log("Response is: {}".format(str(response)))
       return response

   def _each_result(self, document):
  	 if self.lastmodified:
		self._each_result_last_modified(document)
	 else:
		self._each_result_normal(document)
   def _each_result_last_modified(self, document):
      current_record=self.db.connection(self.db.connection.wcp_crawls.wcp_page_uri==document.response.uri).select().first()
      if current_record:
	
         now_date = WCPUtility.getDate( document.response.headers['last-modified'] )
      
         if WCPUtility.cmpDate(WCPUtility.getDate(current_record.wcp_last_modified), now_date ) > 0:
             result = self.downloader.download( document )
             if result:
                current_record.wcp_page_modified= now_date
                if current_record.update():
                    self.log.log("Updated record {}".format( current_record.as_dict().__str__()))
                else:
                    self.log.log("Could not Update {}".format( current_record.as_dict().__str__()))
             else:
                self.log.log("Unable to download {}".format( current_record.as_dict().__str__()))
         else:
            self.log.log("Hash was consistent, no update to {}".format( current_record.as_dict().__str__()))
      else:
	   now_date = WCPUtility.getDate( document.response.headers['last-modified'] )

	   self._each_result_save(document, now_date=now_date)
   def _each_result_normal(self, document):
      current_record = self.db.connection(self.db.connection.wcp_crawls.wcp_page_uri==document.response.uri).select().first()
      if not current_record:
	  self._each_result_save( document, now_date=None )
      else:
	  self.log.log("Unable to save result as there is already a record for {}".format( current_record.as_dict().__str__()))
	   
   def _each_result_save( self,document,now_date=None ):
         self.downloader.download( document )
         new_record = self.db.save(wcp_page_uri=document.response.uri, wcp_page_modified=now_date )
         if new_record:
            self.log.log("Saved new record {}".format(new_record.as_dict().__str__()))
         else:
            self.log.log("Could not insert {}".format( new_record.as_dict().__str__()))


   def run(self):
      self.crawl_all_links()
            
class WCPDocument( object ): 
   def __init__(self, response):
      self.response=response
      if response.status == 200:
          self.valid = True
          self.raw = response.getResult()
	  if re.findall(r"""text\/html""", response.headers['content-type']):
		  try:
		     htmlDom = HtmlDom()
		     self.dom = htmlDom.createDom( self.raw )
		  except Exception, e:
		     self.valid = False
          else:
	        self.dom = False
	        self.valid = True
            
      else:
         self.valid = False
            
      

class WCPResponse( object ):
   def __init__(self, status='', data='',uri='', headers=dict()):
      self.status= status
      self.data = data
      self.uri = uri
      self.headers=  headers
   def __str__(self):
      return "STATUS: {0}\r\nHEADERS: {1}".format( self.status, str(self.headers) )
   def getResult(self):
      return self.data
   def _getHeaders(self,headers):
      dictionaryOfHeaders={}
      for i in headers:
         dictionaryOfHeaders[ i[0] ]  = i[1] 
      return dictionaryOfHeaders
   

class WCPRequest( object ):
   @staticmethod 
   def request(uri):
       try: 
           req = urllib2.Request(uri)
           res = urllib2.urlopen(req, timeout=5)
           status = res.getcode()
           data = res.read()## needs root element
           response= WCPResponse(status=status,data=data,uri=uri,headers=res.info().dict)
	   WCPUtility.getLogger().log("Response is: {0}".format(str(response)))
	   return response
       except Exception, exception:
           response = WCPResponse(status=None,data="",uri=uri,headers=[])
	   logger = WCPUtility.getLogger()
	   logger.log("Response was invalid: {0}".format(str(exception)))
	   logger.log("Response is: {0}".format(str( response )))
	   return response

class WCPDownloader( object ):
    def __init__(self,  db): 
        self.db = db
    def download(self, document):
        path =  WCPUtility.getDownloadPath()
        fullname = WCPUtility.getFullDownloadName( document.response.uri )
        try: 
            file= open("{}/{}".format(path,fullname), "w+") 
            file.write(  document.raw )
            file.close()
            return True
        except Exception, e:
	    logger =WCPUtility.getLogger()
	    logger.log("Error while saving file: {0}".format(str(e)))
            return False ## add logging
            
  
def setLogger():
    wcp.logger=WCPLog()

def setPlatformDefaults():
	if platform.system().lower() =="linux":
		wcp.downloadPath = "/usr/local/wcp/downloads/"
		wcp.logPath = "/usr/local/wcp/log"
	elif platform.system().lower() == "windows":
		wcp.downloadPath = "C:/wcp/downloads/"
		wcp.logPath = "C:/wcp/log"

  
def main(args):
    setLogger()
    setPlatformDefaults()
    args = argparse.ArgumentParser("WCP Downloader", add_help=True)
    args.add_argument("--url",help="Add A URL to WCP. This will be used in the crawling of all links", default=False)
    args.add_argument("--lastmodified", help="Use last modified header to check if there are updates", action="store_true", default=False)
    args.add_argument("--daemon", help="Run WCP as a Daemon", action='store_true')
    res = args.parse_args()
    if res.url:
       if res.daemon:
	  if res.lastmodified:
	  	subprocess.Popen(["python", sys.argv[0],"--url", res.url, "--lastmodified"])
       else:
	       crawler = WCPCrawler( entrypoint=res.url, lastmodified=res.lastmodified )
	       crawler.run()
