
import os
import shutil
import platform

try:
   from setuptools  import setup
except Exception, e:
   from distutils.core.setup import setup
from distutils.command.install import install
try:
    from windows_env import manage_registry_env_vars
except Exception, e: ##not windows
    pass




class install_py( install ):
    def run( self ):
        binfile = ""	 
  	prefix = ""
        osfile = ""
	pipcmd = ""

	if platform.system().lower()=="linux":
	   prefix = "/usr/local/wcp/"
	   binfile = os.getcwd()+"/bin/wcp.linux"
	   osfile = "/usr/bin/wcp"
	   pipcmd = "pip install -r './requirements.txt'"
	   self.install_general(binfile, prefix, pipcmd,  osfile )	
	   shutil.copy2( binfile, osfile  )
  	   os.system("ln -s {0} {1}".format(os.getcwd()+"/downloads","/usr/local/wcp/downloads"))
           #os.system("ln -s {0} {1}".format(os.getcwd()+"/bin/wcp", "/usr/bin/wcp"))
	elif platform.system().lower()=="windows":
	   prefix = "C:/wcp"
	   binfile = os.getcwd()+"/bin/wcp.windows"
	   osfile = "C:/wcp/wcp"
	   pipcmd = "C:/Python27/Scripts/pip.exe install -r requirements.txt"
	   self.install_general( binfile, prefix, pipcmd, osfile )
	   shutil.copy2( binfile, osfile ) 
	   #manage_registry_env_vars("PATH", osfile)


	install.run( self )
    def install_general( self, binfile, prefix, pipcmd, osfile ):
	os.system(pipcmd)
	removing_dirs =[ prefix ]
	removing_files=[ osfile ]
	for i in removing_dirs:
	   if os.path.exists ( i ):
	       shutil.rmtree( i )
        os.makedirs( prefix )
        if os.path.exists( osfile ):
	    os.remove(osfile)

 

setup(
     name="wcp",
     maintainer="Nadir Hamid",
     maintainer_email="matrix.nad@gmail.com",
     license="MIT",
     packages=['wcp'],
     cmdclass=dict(install=install_py)
)





