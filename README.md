WCP - Web Crawler Project
========================================================================

WCP is an experiment for simple crawler in Python. There are already many
written in and out of Python however it is intended to be focused in the
recursively crawling of inter-domain links within a web-page. As well
as provide a pre-managed structure for all web pages downloaded.


Usage And Installation
========================================================================

Installing WCP
-----------------------------------------------------------------------

To install WCP 

```pip install -r requirements.txt```

```python setup.py build```

```python setup.py install```



Examples
------------------------------------------------------------------------

Specify a URL to download

```
wcp --url "http://45.79.188.221/tests/test_file_1.html"
```
This recursively download all files listed at test_file_1.html

Run program in background

```
wcp --url "http://example.org/" --daemon
```
This should run WCP as a background programs.Files will be available in /downloads/ of the WCP directory.
Errors will be reported in ./logs/log.txt


for  a full list of commands

```
wcp --help
```


